var Mods = [];
Ti.include('includes/Utils.js');
for (var i = 0; i < 4; i++)
    Ti.include('includes/Mod' + i + '.js');

Titanium.UI.setBackgroundColor('#000');

var tabGroup = Titanium.UI.createTabGroup({height: '5%'}); 

var tabs = [];
var wins = [];

for (var i = 0; i < Mods.length; i++) {
    var win = wins[i] = Titanium.UI.createWindow({  
        title: Mods[i].title
    });
    tabs[i] = Titanium.UI.createTab({ 
        title: Mods[i].title
    });
    tabGroup.addTab(tabs[i]);
    tabs[i].window = wins[i];
    Mods[i].tab = tabs[i];


    if (i == Mods.length - 1) {
        win = Ti.UI.createScrollView({
            contentWidth: 'auto',
            contentHeight: 'auto',
            showVerticalScrollIndicator: true,
            backgroundColor: '#ddd'
        });
        wins[i].add(win);
    }
    Mods[i].main(win);
}

Mods[0].tab.addEventListener('focus', function (e) {
    Mods[0].main(this.window);
});
Mods[1].tab.addEventListener('focus', function (e) {
    Mods[1].main(this.window);
});
Mods[2].tab.addEventListener('focus', function (e) {
    Mods[2].main(this.window);
});
Mods[3].tab.addEventListener('focus', function (e) {
    Mods[3].main(this.window);
});

tabGroup.open();
