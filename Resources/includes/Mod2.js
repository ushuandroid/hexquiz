Mods[2] = {
    title: 'Tips',
    tab: null,
    win: null,
    level: 5,
    colors: [],
    buttons: [],
    main: function (win) {
        this.colors = Utils.genColors(this.level);
        this.showButtons(win);
    },
    showButtons: function (win) {
        var Mod = this;
        for (var i = 0; typeof this.buttons[i] != 'undefined'; i++)
            win.remove(this.buttons[i]);
        var size = 100 / this.level;
        for (var i = 0; i < this.level; i++) {
            this.buttons[i] = Titanium.UI.createLabel({
                color: '#fff',
                shadowColor: '#000',
                shadowOffset: {x:5, y:5},
                backgroundColor: '#' + this.colors[i],
                font: {fontFamily: 'monospace', fontSize: 24},
                top: (size * i) + '%',
                width: '50%',
                height: size + '%',
                textAlign: Ti.UI.TEXT_ALIGNMENT_CENTER,
                text: '#' + this.colors[i]
            });
            this.buttons[i].addEventListener('click', function (e) {
                Ti.UI.Clipboard.setText(this.text.substring(1));
            });
            win.add(this.buttons[i]);
        }
    }
};
