Mods[0] = {
    title: 'Hexa',
    tab: null,
    level: 5,
    position: 0,
    colors: [],
    buttons: [],
    main: function (win) {
        this.position = Math.floor(Math.random() * this.level);
        this.colors = Utils.genColors(this.level);
        win.backgroundColor = '#' + this.colors[this.position];
        this.showButtons(win);
    },
    showButtons: function (win) {
        var Mod = this;
        for (var i = 0; typeof this.buttons[i] != 'undefined'; i++)
            win.remove(this.buttons[i]);
        var size = 100 / this.level;
        for (var i = 0; i < this.level; i++) {
            var button = Titanium.UI.createLabel({
                color: '#ddd',
                backgroundColor: '#222',
                font: {fontFamily: 'monospace', fontSize: 24},
                top: (size * i) + '%',
                width: '50%',
                height: size + '%',
                textAlign: Ti.UI.TEXT_ALIGNMENT_CENTER,
                text: '#' + this.colors[i]
            });
            button.addEventListener('click', function (e) {
                if (this.text.indexOf('OK ') == 0)
                    Mod.main(win);
                else {
                    Ti.UI.Clipboard.setText(this.text.substring(1));
                    if (win.backgroundColor == this.text)
                        this.text = 'OK ' + this.text;
                    else
                        this.backgroundColor = this.text;
                }
            });
            this.buttons[i] = button;
            win.add(button);
        };
    }
};
