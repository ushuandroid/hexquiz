Mods[1] = {
    title: 'Color',
    tab: null,
    level: 5,
    position: 0,
    colors: [],
    buttons: [],
    label: {},
    main: function (win) {
        this.position = Math.floor(Math.random() * this.level);
        this.colors = Utils.genColors(this.level);
        this.showButtons(win);
    },
    showButtons: function (win) {
        var Mod = this;
        for (var i = 0; typeof this.buttons[i] != 'undefined'; i++)
            win.remove(this.buttons[i]);
        var size = 100 / (this.level + 1);
        this.label = Titanium.UI.createLabel({
            color: '#000',
            backgroundColor: '#fff',
            font: {fontFamily: 'monospace', fontSize: 26},
            top: '0%',
            width: '50%',
            height: size + '%',
            textAlign: Ti.UI.TEXT_ALIGNMENT_CENTER,
            text: '#' + this.colors[this.position]
        });
        win.add(this.label);
        for (var i = 0; i < this.level; i++) {
            var button = Titanium.UI.createLabel({
                backgroundColor: '#' + this.colors[i],
                textAlign: Ti.UI.TEXT_ALIGNMENT_CENTER,
                color: '#fff',
                font: {fontFamily: 'monospace', fontSize: 24},
                width: '50%',
                height: size + '%',
                top: (size * (i + 1)) + '%',
                text: ''
            });
            button.addEventListener('click', function (e) {
                if (this.backgroundColor != '#222') {
                    Ti.UI.Clipboard.setText(this.backgroundColor.substring(1));
                    if (Mod.label.text == this.backgroundColor) {
                        if (this.text == 'OK')
                            Mod.main(win);
                        else
                            this.text = 'OK';
                    }
                    else {
                        this.text = this.backgroundColor;
                        this.color = '#ddd';
                        this.backgroundColor = '#222';
                    }
                }
            });
            this.buttons[i] = button;
            win.add(button);
        }
    }
};
