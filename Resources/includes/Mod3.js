Mods[3] = {
    title: 'About',
    tab: null,
    level: 5,
    slider: null,
    main: function (win) {
        if (this.slider == null) {
            this.slider = Titanium.UI.createSlider({
                width: '100%',
                min: 2,
                max: 16,
                value: this.level
            });

            this.slider.addEventListener('change', function(e) {
                this.level = Math.round(e.value);
                for (var i = 0; i < 3; i++)
                    Mods[i].level = this.level;
            });
            win.add(this.slider);
        }
    }
};
