Utils = {
    genColor: function () {
        var hexaRand = Math.floor(Math.random() * 16777216).toString(16);
        for (var i = hexaRand.length; i < 6; i++)
            hexaRand = '0' + hexaRand;
        return hexaRand;
    },
    genColors: function (level) {
        var colors = [];
        for (var i = 0; i < level; i++)
            colors[i] = this.genColor();
        return colors;
    }
};
